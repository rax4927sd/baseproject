<?php

namespace Drupal\rax_order\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\Language;

/**
 * Form controller for the rax_order entity edit forms.
 */
class RaxOrderForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\rax_order\Entity\RaxOrder $entity */
    $form = parent::form($form, $form_state);
    $rax_order = $this->entity;
    $form['#tree'] = TRUE;
    $form['#theme'] = 'rax_order_edit_form';
    dsm($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The order %feed has been updated.', ['%feed' => $entity->toLink()->toString()]), FALSE);
    }
    else {
      $this->messenger()->addStatus($this->t('The order %feed has been added.', ['%feed' => $entity->toLink()->toString()]), FALSE);
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

  /**
   * Builds a read-only form element for a field.
   *
   * @param string $label
   *   The element label.
   * @param string $value
   *   The element value.
   *
   * @return array
   *   The form element.
   */
  protected function fieldAsReadOnly($label, $value) {
    return [
      '#type' => 'item',
      '#wrapper_attributes' => [
        'class' => ['container-inline'],
      ],
      '#title' => $label,
      '#markup' => $value,
    ];
  }

}

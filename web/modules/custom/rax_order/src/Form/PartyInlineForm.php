<?php

namespace Drupal\rax_order\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\inline_entity_form\Form\EntityInlineForm;

/**
 * Defines the inline form for party.
 */
class PartyInlineForm extends EntityInlineForm {

  /**
   * The loaded parties types.
   *
   * @var \Drupal\rax_order\Entity\PartyTypeInterface[]
   */
  protected $partyTypes;

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeLabels() {
    $labels = [
      'singular' => $this->t('party'),
      'plural' => $this->t('parties'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = parent::getTableFields($bundles);
    $fields['label']['label'] = $this->t('Title');
    return $fields;
  }

}

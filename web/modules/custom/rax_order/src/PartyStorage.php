<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the party storage.
 */
class PartyStorage extends SqlContentEntityStorage implements PartyStorageInterface {

}

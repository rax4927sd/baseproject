<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for party storage.
 */
interface PartyStorageInterface extends ContentEntityStorageInterface {

}

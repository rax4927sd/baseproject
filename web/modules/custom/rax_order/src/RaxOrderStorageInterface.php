<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for order storage.
 */
interface RaxOrderStorageInterface extends ContentEntityStorageInterface {

}

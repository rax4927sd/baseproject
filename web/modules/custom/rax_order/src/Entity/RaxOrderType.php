<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Rax Order Type.
 *
 * @ConfigEntityType(
 *   id = "rax_order_type",
 *   label = @Translation("Order Type", context = "RaxOrder"),
 *   bundle_of = "rax_order",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "rax_order_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "workflow",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\rax_order\Form\RaxOrderTypeEntityForm",
 *       "add" = "Drupal\rax_order\Form\RaxOrderTypeEntityForm",
 *       "edit" = "Drupal\rax_order\Form\RaxOrderTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\rax_order\RaxOrderTypeListBuilder",
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/ddap-rax/config/rax-order-type/{rax_order_type}",
 *     "add-form" = "/admin/ddap-rax/config/rax-order-type/add",
 *     "edit-form" = "/admin/ddap-rax/config/rax-order-type/{rax_order_type}/edit",
 *     "delete-form" = "/admin/ddap-rax/config/rax-order-type/{rax_order_type}/delete",
 *     "collection" = "/admin/ddap-rax/config/rax-order-type/list",
 *   }
 * )
 */
class RaxOrderType extends ConfigEntityBundleBase {

  /**
   * The order type workflow ID.
   *
   * @var string
   */
  protected $workflow;

  /**
   * {@inheritdoc}
   */
  public function getWorkflowId() {
    return $this->workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function setWorkflowId($workflow_id) {
    $this->workflow = $workflow_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // The order type must depend on the module that provides the workflow.
    $workflow_manager = \Drupal::service('plugin.manager.workflow');
    $workflow = $workflow_manager->createInstance($this->getWorkflowId());
    $this->calculatePluginDependencies($workflow);

    return $this;
  }

}

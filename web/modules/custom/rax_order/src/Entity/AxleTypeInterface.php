<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the interface for axle types.
 */
interface AxleTypeInterface extends ConfigEntityBundleBase {

}

<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Party entity class.
 *
 * @ContentEntityType(
 *   id = "party",
 *   label = @Translation("Party", context = "RaxOrder"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\rax_order\PartyAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *     "inline_form" = "Drupal\rax_order\Form\PartyInlineForm",
 *   },
 *   base_table = "party",
 *   admin_permission = "administer party entity",
 *   fieldable = TRUE,
 *   field_indexes = {
 *     "party_id"
 *   },
 *   entity_keys = {
 *     "id" = "party_id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "bundle" = "type"
 *   },
 *   bundle_entity_type = "party_type",
 *   field_ui_base_route = "entity.party_type.edit_form"
 * )
 */
class Party extends ContentEntityBase implements PartyInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getAxles() {
    return $this->get('axles')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setAxles(array $parties) {
    $this->set('axles', $parties);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAxles() {
    return !$this->get('axles')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function addAxle(AxleInterface $axle) {
    if (!$this->hasAxle($axle)) {
      $this->get('axles')->appendItem($axle);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeAxle(AxleInterface $axle) {
    $index = $this->getItemIndex($axle);
    if ($index !== FALSE) {
      $this->get('axles')->offsetUnset($axle);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAxle(AxleInterface $axle) {
    return $this->getItemIndex($axle) !== FALSE;
  }

  /**
   * Gets the index of the given axle.
   *
   * @param \Drupal\rax_order\Entity\AxleInterface $axle
   *   The axle.
   *
   * @return int|bool
   *   The index of the given axle, or FALSE if not found.
   */
  protected function getItemIndex(AxleInterface $axle) {
    $values = $this->get('axles')->getValue();
    $axle_ids = array_map(function ($value) {
      return $value['target_id'];
    }, $values);

    return array_search($axle->id(), $axle_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->get('order_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key, $default = NULL) {
    $data = [];
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
    }
    return isset($data[$key]) ? $data[$key] : $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setData($key, $value) {
    $this->get('data')->__set($key, $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetData($key) {
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
      unset($data[$key]);
      $this->set('data', $data);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each axles.
    foreach ($this->getAxles() as $axle) {
      if ($axle->party_id->isEmpty()) {
        $axle->party_id = $this->id();
        $axle->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Delete the axles of a deleted order.
    $axles = [];
    /** @var \Drupal\rax_order\Entity\PartyInterface $entity */
    foreach ($entities as $entity) {
      foreach ($entity->getAxles() as $axle) {
        $axles[$axle->id()] = $axle;
      }
    }
    /** @var \Drupal\rax_order\AxleStorageInterface $axle_storage */
    $axle_storage = \Drupal::service('entity_type.manager')->getStorage('axle');
    $axle_storage->delete($axles);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The order backreference, populated by Order::postSave().
    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setDescription(t('The parent order.'))
      ->setSetting('target_type', 'rax_order')
      ->setReadOnly(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The party title.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 512,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['axles'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Axles'))
      ->setDescription(t('The party axles.'))
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'axle')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 0,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('axle'),
          'label_plural' => t('axles'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'axles_table',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the party was created.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the party was last edited.'))
      ->setRequired(TRUE);

    return $fields;
  }

}

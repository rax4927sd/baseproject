<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Axle Type.
 *
 * @ConfigEntityType(
 *   id = "axle_type",
 *   label = @Translation("Axle Type", context = "RaxOrder"),
 *   bundle_of = "axle",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "axle_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\rax_order\Form\AxleTypeEntityForm",
 *       "add" = "Drupal\rax_order\Form\AxleTypeEntityForm",
 *       "edit" = "Drupal\rax_order\Form\AxleTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\rax_order\AxleTypeListBuilder",
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/ddap-rax/config/axle-type/{axle_type}",
 *     "add-form" = "/admin/ddap-rax/config/axle-type/add",
 *     "edit-form" = "/admin/ddap-rax/config/axle-type/{axle_type}/edit",
 *     "delete-form" = "/admin/ddap-rax/config/axle-type/{axle_type}/delete",
 *     "collection" = "/admin/ddap-rax/config/axle-type/list",
 *   }
 * )
 */
class AxleType extends ConfigEntityBundleBase {}

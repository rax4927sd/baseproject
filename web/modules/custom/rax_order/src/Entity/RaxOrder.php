<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Rax Order entity class.
 *
 * @ContentEntityType(
 *   id = "rax_order",
 *   label = @Translation("Order", context = "RaxOrder"),
 *   label_collection = @Translation("Orders", context = "RaxOrder"),
 *   label_singular = @Translation("order", context = "RaxOrder"),
 *   label_plural = @Translation("orders", context = "RaxOrder"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order",
 *     plural = "@count orders",
 *     context = "RaxOrder",
 *   ),
 *   bundle_label = @Translation("Order type", context = "RaxOrder"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rax_order\RaxOrderListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "storage" = "Drupal\rax_order\RaxOrderStorage",
 *     "form" = {
 *       "default" = "Drupal\rax_order\Form\RaxOrderForm",
 *       "add" = "Drupal\rax_order\Form\RaxOrderForm",
 *       "edit" = "Drupal\rax_order\Form\RaxOrderForm",
 *       "delete" = "Drupal\rax_order\Form\RaxOrderDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\rax_order\RaxOrderRouteProvider"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "access" = "Drupal\rax_order\RaxOrderAccessControlHandler",
 *   },
 *   base_table = "rax_order",
 *   admin_permission = "administer rax_order entity",
 *   fieldable = TRUE,
 *   field_indexes = {
 *     "order_number"
 *   },
 *   entity_keys = {
 *     "id" = "order_id",
 *     "label" = "order_number",
 *     "uuid" = "uuid",
 *     "bundle" = "type"
 *   },
 *   links = {
 *     "canonical" = "/admin/ddap-rax/orders/{rax_order}",
 *     "add-page" = "/admin/ddap-rax/orders/add",
 *     "add-form" = "/admin/ddap-rax/orders/add/{rax_order_type}",
 *     "edit-form" = "/admin/ddap-rax/orders/{rax_order}/edit",
 *     "delete-form" = "/admin/ddap-rax/orders/{rax_order}/delete",
 *     "collection" = "/admin/ddap-rax/orders",
 *   },
 *   bundle_entity_type = "rax_order_type",
 *   field_ui_base_route = "entity.rax_order_type.edit_form"
 * )
 */
class RaxOrder extends ContentEntityBase implements RaxOrderInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getAxlesCount() {
    return $this->get('axles_count');
  }

  /**
   * {@inheritdoc}
   */
  public function setAxlesCount(int $count) {
    $this->set('axles_count', $count);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderNumber() {
    return $this->get('order_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderNumber($order_number) {
    $this->set('order_number', $order_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParties() {
    return $this->get('parties')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setParties(array $parties) {
    $this->set('parties', $parties);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasParties() {
    return !$this->get('parties')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function addParty(PartyInterface $party) {
    if (!$this->hasParty($party)) {
      $this->get('parties')->appendItem($party);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeParty(PartyInterface $party) {
    $index = $this->getItemIndex($party);
    if ($index !== FALSE) {
      $this->get('parties')->offsetUnset($index);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasParty(PartyInterface $party) {
    return $this->getItemIndex($party) !== FALSE;
  }

  /**
   * Gets the index of the given party.
   *
   * @param \Drupal\rax_order\Entity\PartyInterface $party
   *   The order item.
   *
   * @return int|bool
   *   The index of the given order item, or FALSE if not found.
   */
  protected function getItemIndex(PartyInterface $party) {
    $values = $this->get('parties')->getValue();
    $party_ids = array_map(function ($value) {
      return $value['target_id'];
    }, $values);

    return array_search($party->id(), $party_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getItems() {
    return $this->get('parties')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each order item.
    foreach ($this->getItems() as $parties) {
      if ($parties->order_id->isEmpty()) {
        $parties->order_id = $this->id();
        $parties->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Delete the parties of a deleted order.
    $parties = [];
    /** @var \Drupal\rax_order\Entity\RaxOrderInterface $entity */
    foreach ($entities as $entity) {
      foreach ($entity->getItems() as $party) {
        $parties[$party->id()] = $party;
      }
    }
    /** @var \Drupal\rax_order\PartyStorageInterface $party_storage */
    $party_storage = \Drupal::service('entity_type.manager')->getStorage('party');
    $party_storage->delete($parties);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['order_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order number'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['axles_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of axles'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('min', 0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Status'))
      ->setDescription(t('The order state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow_callback',
      ['\Drupal\rax_order\Entity\RaxOrder', 'getWorkflowId']);

    $fields['parties'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parties'))
      ->setDescription(t('The order parties.'))
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'party')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 0,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('party'),
          'label_plural' => t('parties'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'parties_table',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the order was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the order was last edited.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['lead_time'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Planned lead time'))
      ->setDescription(t('Planned lead time.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Owner field of the rax order.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\rax_order\Entity\RaxOrderInterface $order
   *   The order.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(RaxOrderInterface $order) {
    $workflow = RaxOrderType::load($order->bundle())->getWorkflowId();
    return $workflow;
  }

}

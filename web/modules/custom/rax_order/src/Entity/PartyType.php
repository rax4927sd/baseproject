<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Party Type.
 *
 * @ConfigEntityType(
 *   id = "party_type",
 *   label = @Translation("Party Type", context = "RaxOrder"),
 *   bundle_of = "party",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "party_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\rax_order\Form\PartyTypeEntityForm",
 *       "add" = "Drupal\rax_order\Form\PartyTypeEntityForm",
 *       "edit" = "Drupal\rax_order\Form\PartyTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\rax_order\PartyTypeListBuilder",
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/ddap-rax/config/party-type/{party_type}",
 *     "add-form" = "/admin/ddap-rax/config/party-type/add",
 *     "edit-form" = "/admin/ddap-rax/config/party-type/{party_type}/edit",
 *     "delete-form" = "/admin/ddap-rax/config/party-type/{party_type}/delete",
 *     "collection" = "/admin/ddap-rax/config/party-type/list",
 *   }
 * )
 */
class PartyType extends ConfigEntityBundleBase {}

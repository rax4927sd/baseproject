<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the interface for party types.
 */
interface PartyTypeInterface extends ConfigEntityBundleBase {

}

<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the interface for Axle.
 */
interface AxleInterface extends ContentEntityInterface {

  /**
   * Gets the parent party.
   *
   * @return \Drupal\rax_order\Entity\Party|null
   *   The order, or NULL.
   */
  public function getParty();

  /**
   * Gets the parent party ID.
   *
   * @return int|null
   *   The order ID, or NULL.
   */
  public function getPartyId();

  /**
   * Gets the axle title.
   *
   * @return string
   *   The axle title
   */
  public function getTitle();

  /**
   * Sets the axle title.
   *
   * @param string $title
   *   The axle title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets an order item data value with the given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *   The value.
   */
  public function getData($key, $default = NULL);

  /**
   * Sets an order item data value with the given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   *
   * @return $this
   */
  public function setData($key, $value);

  /**
   * Unsets an order item data value with the given key.
   *
   * @param string $key
   *   The key.
   *
   * @return $this
   */
  public function unsetData($key);

  /**
   * Gets the order item creation timestamp.
   *
   * @return int
   *   The order item creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the order item creation timestamp.
   *
   * @param int $timestamp
   *   The order item creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}

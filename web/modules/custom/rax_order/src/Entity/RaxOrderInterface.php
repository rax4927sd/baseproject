<?php

namespace Drupal\rax_order\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for Rax Order.
 */
interface RaxOrderInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the order number.
   *
   * @return string
   *   The order number.
   */
  public function getOrderNumber();

  /**
   * Set the order number.
   *
   * @param string $order_number
   *   The order number.
   *
   * @return $this
   */
  public function setOrderNumber($order_number);

  /**
   * Gets the parties.
   *
   * @return \Drupal\rax_order\Entity\PartyInterface[]
   *   The Parties.
   */
  public function getParties();

  /**
   * Sets the parties.
   *
   * @param \Drupal\rax_order\Entity\PartyInterface[] $parties
   *   The Parties.
   *
   * @return $this
   */
  public function setParties(array $parties);

  /**
   * Gets whether the order has parties.
   *
   * @return bool
   *   TRUE if the order has parties, FALSE otherwise.
   */
  public function hasParties();

  /**
   * Adds a Party.
   *
   * @param \Drupal\rax_order\Entity\PartyInterface $party
   *   The Party.
   *
   * @return $this
   */
  public function addParty(PartyInterface $party);

  /**
   * Removes a Party.
   *
   * @param \Drupal\rax_order\Entity\PartyInterface $party
   *   The Party.
   *
   * @return $this
   */
  public function removeParty(PartyInterface $party);

  /**
   * Checks whether the order has a given Party.
   *
   * @param \Drupal\rax_order\Entity\PartyInterface $party
   *   The Party.
   *
   * @return bool
   *   TRUE if the Party was found, FALSE otherwise.
   */
  public function hasParty(PartyInterface $party);

  /**
   * Set the number of order axles.
   *
   * @param int $count
   *   The quantity of axles.
   *
   * @return $this
   */
  public function setAxlesCount(int $count);

  /**
   * Get the number of order axles.
   *
   * @return int
   *   The quantity of axles.
   */
  public function getAxlesCount();

}

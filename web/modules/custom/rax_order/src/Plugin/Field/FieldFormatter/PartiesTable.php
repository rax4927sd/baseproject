<?php

namespace Drupal\rax_order\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'parties_table' formatter.
 *
 * @FieldFormatter(
 *   id = "parties_table",
 *   label = @Translation("Party table"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class PartiesTable extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\rax_order\Entity\RaxOrderInterface $order */
    $order = $items->getEntity();
    $elements = [];
    $elements[0] = [
      '#type' => 'view',
      // @todo Allow the view to be configurable.
      '#name' => 'parties_table',
      '#arguments' => [$order->id()],
      '#embed' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();
    return $entity_type == 'rax_order' && $field_name == 'parties';
  }

}

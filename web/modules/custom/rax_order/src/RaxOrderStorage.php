<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the order storage.
 */
class RaxOrderStorage extends SqlContentEntityStorage implements RaxOrderStorageInterface {

}

<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for axle storage.
 */
interface AxleStorageInterface extends ContentEntityStorageInterface {

}

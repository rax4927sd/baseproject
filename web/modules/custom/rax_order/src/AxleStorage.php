<?php

namespace Drupal\rax_order;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the axle storage.
 */
class AxleStorage extends SqlContentEntityStorage implements AxleStorageInterface {

}
